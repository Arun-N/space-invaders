// * GLOBAL TODO: and FIXME:
// TODO 1. Add Audio.
// TODO 2. Cleanup the code.
// !    3. Write code to prevent keybindings (except for ENTER) from firing before the game starts and after the game ends.
// !    4. Enemy projectile start position is wrong. More pronounced during last 2 or 1 row(s) are left
// TODO 5. Increase enemy movement speed after each row is destroyed (11 enemies are killed)
// ?    6. Should enemy speed increment be based on every single enemy destroyed OR each row destroyed?
// TODO 7. Scoring system + save highscore in browser
// TODO 8. Fade in screen to Game Over page after player dies / destroys all enemies
// TODO 9. Spawn motherships after certain intervals only if # of enemies left > 8

// * NOTE: In space invaders killing all enemies will yield a fixed score. Score can be increased further by killing the motherships

$(document).ready(function () {
  // ---------- utility constants ----------
  const NUM_OF_ENEMIES = 23;
  const left_right_max_limit = 200; // in pixels
  const screen_height = window.innerHeight;
  const screen_width = window.innerWidth;
  // ---------- utility variables ----------
  let loader = null;
  // ---------- enemy stuff ----------
  let current_horizontal_position = 0; // in pixels
  let horizontal_difference = 40; // in pixels
  let current_vertical_position = -80; // in pixels
  let vertical_difference = 40; // in pixels
  let current_direction = "left";
  let movement_speed = 1500; // in ms (milliseconds)
  let enemy_proj_speed = 5;
  let enemy_movement; // for holding enemy movement interval reference
  let enemy_projectile_polling_interval;
  // ---------- player stuff ----------
  let player_movement_speed = 30; // in pixels per keypress
  let player_proj_speed = 40;

  // ---------------- Game Objects and References ----------------
  let PLAYER = null; // Contains the Player() object
  let PLAYER_PROJECTILES_COUNT = 0;
  let ENEMY = []; // Contains Enemy() objects
  let ENEMY_PROJECTILES_COUNT = 0;

  // =============================================================================================
  // ====================================== RESOURCE LOADER ======================================
  // =============================================================================================
  // * resources {'foo': {'path': 'Resources/Sprites/game/bar.png'}}
  let Loader = {
    resources: {},
  };

  // ====================================================================================
  // ====================================== PLAYER ======================================
  // ====================================================================================

  function Player() {
    let element = $("#player-character");
    let lives = 3;

    // Setup the player sprite
    console.log(loader.resources.player);
    $(".player").append(
      `<img src="${loader.resources.player.path}" alt="player">`
    );

    function getPosition() {
      // Return 'left' css position [numeric type]
      return parseInt($(element).css("left"));
    }

    function getBoundingBox() {
      let bounding_box = element.get(0).getBoundingClientRect();
      return {
        x: bounding_box.x,
        y: bounding_box.y,
        width: bounding_box.width,
        height: bounding_box.height,
      };
    }

    function setPosition(newPos) {
      // Sets the 'left' css prop. [newPos => numeric type]
      $(element).css("left", newPos + "px");
    }

    function getRemainingLives() {
      // Retrieves the # of lives remaining
      return lives;
    }

    function decreaseLifeByOne() {
      lives -= 1;
      return lives;
    }

    return {
      getPosition: getPosition,
      setPosition: setPosition,
      getRemainingLives: getRemainingLives,
      getBoundingBox: getBoundingBox,
      decreaseLifeByOne: decreaseLifeByOne,
      element: element,
    };
  }

  // ===============================================================================================
  // ====================================== PLAYER PROJECTILE ======================================
  // ===============================================================================================

  function PlayerProjectile() {
    let element = null;
    let id = `player_proj_${PLAYER_PROJECTILES_COUNT++}`;
    let movement_interval = null;
    let collision_interval = null;

    // Add sprite to scene
    $(".game-container").append(`
        <div class="player-projectile" id="${id}" style="bottom: 5%; left: calc(${PLAYER.getPosition()}px + 25px)">
            <img src="${
              loader.resources.projectile.path
            }" alt="player_projectile">
        </div>`);

    element = document.getElementById(id);

    function getPosition() {
      // Return 'bottom' css position [numeric type]
      return parseInt($(element).css("bottom"));
    }

    function getBoundingBox() {
      let bounding_box = element.getBoundingClientRect();
      return {
        x: bounding_box.x,
        y: bounding_box.y,
        width: bounding_box.width,
        height: bounding_box.height,
      };
    }

    function setPosition(newPos) {
      // Sets the 'bottom' css prop. [newPos => numeric type]
      $(element).css("bottom", newPos + "px");
    }

    movement_interval = setInterval(function () {
      // Player Projectile moves upwards
      setPosition(getPosition() + player_proj_speed);
      if (getPosition() >= screen_height + 100) {
        $(document).trigger("deletePlayerProjectile", [
          element,
          movement_interval,
          collision_interval,
        ]);
      }
    }, 10);

    collision_interval = setInterval(function () {
      // Collision detection interval in which we loop through all Enemy() obj and detect collision using Axis-Aligned Bounding Box method
      ENEMY.forEach((enemy, index) => {
        let enemy_box = enemy.getPosition();
        let proj_box = element.getBoundingClientRect();

        if (
          enemy_box.x < proj_box.x + proj_box.width &&
          enemy_box.x + enemy_box.width > proj_box.x &&
          enemy_box.y < proj_box.y + proj_box.height &&
          enemy_box.y + enemy_box.height > proj_box.y
        ) {
          $(document).trigger("deleteEnemy", [enemy, index]);
          $(document).trigger("deletePlayerProjectile", [
            element,
            movement_interval,
            collision_interval,
          ]);
          clearInterval(collision_interval);
        }
      });
    }, 10);
  }

  // ===================================================================================
  // ====================================== ENEMY ======================================
  // ===================================================================================

  function Enemy(enemy_sprite_src) {
    let element = null;

    // * Initializing enemy by creating an <img> element with the provided sprite src
    let enemy = document.createElement("img");
    enemy.src = enemy_sprite_src;
    enemy.alt = "enemy";
    $(".enemy-div").append(enemy);
    element = enemy;

    function getPosition() {
      // Return current x, y position along with width and height
      let bounding_box = element.getBoundingClientRect();
      return {
        x: bounding_box.x,
        y: bounding_box.y,
        width: bounding_box.width,
        height: bounding_box.height,
      };
    }

    return {
      getPosition: getPosition,
      element: element,
    };
  }

  // ===============================================================================================
  // ====================================== ENEMY PROJECTILE ======================================
  // ===============================================================================================

  function EnemyProjectile(left, top) {
    console.log(top);
    let element = null;
    let id = `enemy_proj_${ENEMY_PROJECTILES_COUNT++}`;
    let movement_interval = null;
    let collision_interval = null;

    // Add sprite to scene
    // * 16/2 is used as an offset for horizontal center of the sprite
    // * +20 for top is just a padding between projectile and enemy sprite
    $(".game-container").append(`
        <div class="enemy-projectile" id="${id}" style="top: ${top}px; left: ${left}px; transform: rotate(180deg)">
            <img src="${loader.resources.projectile.path}" alt="player_projectile">
        </div>`);

    element = document.getElementById(id);

    function getPosition() {
      // Return 'top' css position [numeric type]
      return parseInt($(element).css("top"));
    }

    function setPosition(newPos) {
      // Sets the 'top' css prop. [newPos => numeric type]
      $(element).css("top", newPos + "px");
    }

    // todo: stopping this for a sec
    movement_interval = setInterval(function () {
      // Enemy Projectile moves downwards
      setPosition(getPosition() + enemy_proj_speed);
      if (getPosition() >= 1000) {
        $(document).trigger("deleteEnemyProjectile", [
          element,
          movement_interval,
        ]);
      }
    }, 10);

    // On collision decrease health by 1 and destroy this projectile if health === 0.
    collision_interval = setInterval(function () {
      // Collision detection interval in which we detect collision between this projectile and PLAYER using Axis-Aligned Bounding Box method
      let player_box = PLAYER.getBoundingBox();
      let proj_box = element.getBoundingClientRect();

      if (
        player_box.x < proj_box.x + proj_box.width &&
        player_box.x + player_box.width > proj_box.x &&
        player_box.y < proj_box.y + proj_box.height &&
        player_box.y + player_box.height > proj_box.y
      ) {
        // Decrease player life and remove one from the indicator (top left corner)
        PLAYER.decreaseLifeByOne();
        $(".player_life_indicator").get(0).remove();

        // Delete player if no life left
        if (PLAYER.getRemainingLives() === 0) {
          $(document).trigger("deletePlayer");
        }

        $(document).trigger("deleteEnemyProjectile", [
          element,
          movement_interval,
        ]);
        clearInterval(collision_interval);
      }
    }, 10);

    return {
      getPosition: getPosition,
      setPosition: setPosition,
    };
  }

  // ---------------- Game Events ----------------
  $(document).on("deletePlayer", function (event) {
    // Called when Player() lives === 0
    // TODO: Clean Up & show game over screen
    // 1. Stop Enemy Movement ✅
    // 2. Stop EnemyProjectile instantiations✅
    // 3. Score Screen after transition
    PLAYER.element.get(0).remove();
    clearInterval(enemy_movement);
    clearInterval(enemy_projectile_polling_interval);

    setTimeout(() => {
      $(".game-over").css("display", "block");
      // TODO: Opacity to full black
    }, 1000);
  });

  $(document).on("deletePlayerProjectile", function (
    event,
    proj_element,
    movement_intvl_ref,
    collision_intvl_ref
  ) {
    {
      PLAYER_PROJECTILES_COUNT -= 1;
      $(proj_element).remove();
      clearInterval(movement_intvl_ref);
      clearInterval(collision_intvl_ref);
    }
  });

  $(document).on("deleteEnemy", function (event, enemy, index) {
    {
      enemy.element.remove();
      ENEMY.splice(index, 1);
    }
  });

  $(document).on("deleteEnemyProjectile", function (
    event,
    proj_element,
    movement_intvl_ref
  ) {
    $(proj_element).remove();
    clearInterval(movement_intvl_ref);
  });

  console.log(
    "👾 Welcome to SPACE INVADERS in JS! 👾\n   created by - Arun J. Nair"
  );

  // Handling all the key events ==> arrows, space, enter. Use the keydown event below
  $(document).keydown(function (keydown_event) {
    // NOTE: We are using only the left css property for player positioning
    switch (keydown_event.which) {
      case 37:
        // Left arrow (move left)
        PLAYER.setPosition(PLAYER.getPosition() - player_movement_speed);
        break;
      case 39:
        // Right arrow (move right)
        PLAYER.setPosition(PLAYER.getPosition() + player_movement_speed);
        break;
      case 32:
        // Space (fire button)
        if (PLAYER_PROJECTILES_COUNT < 1) new PlayerProjectile();
        break;
      case 13:
        // =============== Enter key for starting the game ===============
        $(".game-container").find("p").remove();

        // Loading resources
        loader = Object.create(Loader);
        loader.resources.player = {
          path: "Resources/Sprites/game/player.png",
        };
        loader.resources.enemy1 = {
          path: "Resources/Sprites/game/enemy-01.gif",
        };
        loader.resources.enemy2 = {
          path: "Resources/Sprites/game/enemy-02.gif",
        };
        loader.resources.enemy3 = {
          path: "Resources/Sprites/game/enemy-03.gif",
        };
        loader.resources.enemy4 = {
          path: "Resources/Sprites/game/enemy-04.gif",
        };
        loader.resources.projectile = {
          path: "Resources/Sprites/game/projectile.png",
        };

        //Instantiating the player
        PLAYER = new Player();

        // Instantiating the enemies
        for (let i = 0; i < NUM_OF_ENEMIES; i++) {
          let sprite_src = "";
          if (i < 11) {
            sprite_src = loader.resources.enemy1.path;
          } else if (i < 22) {
            sprite_src = loader.resources.enemy2.path;
          } else if (i < 33) {
            sprite_src = loader.resources.enemy4.path;
          } else if (i < 44) {
            sprite_src = loader.resources.enemy3.path;
          } else {
            sprite_src = loader.resources.enemy3.path;
          }
          let enemy = new Enemy(sprite_src);
          ENEMY.push(enemy);
        }

        console.log(ENEMY);

        $(".enemy-div").css("display", "flex");

        // todo: stopping this for a sec
        enemy_movement = setInterval(function () {
          current_horizontal_position =
            current_direction === "left"
              ? current_horizontal_position + horizontal_difference
              : current_horizontal_position - horizontal_difference;
          if (
            current_horizontal_position > left_right_max_limit ||
            current_horizontal_position < -left_right_max_limit
          ) {
            // Switch directions and shift below
            current_direction = current_direction === "left" ? "right" : "left";
            current_vertical_position += vertical_difference;
            $(".enemy-div").css("top", current_vertical_position);
            current_horizontal_position =
              current_direction === "left"
                ? current_horizontal_position + horizontal_difference
                : current_horizontal_position - horizontal_difference;
          } else {
            $(".enemy-div").css("left", current_horizontal_position);
          }
        }, movement_speed);

        let enemy = ENEMY[Math.floor(Math.random() * ENEMY.length)];
        let enemy_pos = enemy.getPosition();
        let ep = new EnemyProjectile(enemy_pos.x, enemy_pos.y);

        // Running an interval to fire enemy projectiles
        // todo: stopping this for a sec
        enemy_projectile_polling_interval = setInterval(() => {
          let enemy = ENEMY[Math.floor(Math.random() * ENEMY.length)];
          let enemy_pos = enemy.getPosition();
          let ep = new EnemyProjectile(enemy_pos.x, enemy_pos.y);
        }, 1000);

        break;
      default:
        break;
    }
  });

  // TODO: NOTE: For Dev purposes. Use to bypass initial screen
  // Instantiating the enemies
  // for (let i = 0; i < NUM_OF_ENEMIES; i++) {
  //     let enemy01 = document.createElement('img');
  //     enemy01.src = "Resources/Sprites/game/enemy-01.gif";
  //     enemy01.alt = "enemy";
  //     $('.enemy-div').append(enemy01);
  // }

  // $('.enemy-div').css('display', 'flex');

  // let enemy_movement = setInterval(function () {
  //     current_horizontal_position = current_direction === 'left' ? current_horizontal_position + horizontal_difference : current_horizontal_position - horizontal_difference;
  //     if (current_horizontal_position > left_right_max_limit || current_horizontal_position < -left_right_max_limit) {
  //         // Switch directions and shift below
  //         current_direction = current_direction === 'left' ? 'right' : 'left';
  //         current_vertical_position += vertical_difference;
  //         $('.enemy-div').css('top', current_vertical_position);
  //         current_horizontal_position = current_direction === 'left' ? current_horizontal_position + horizontal_difference : current_horizontal_position - horizontal_difference;
  //     } else {
  //         $('.enemy-div').css('left', current_horizontal_position);
  //     }
  // }, movement_speed)
});
